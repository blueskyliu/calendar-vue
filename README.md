# 安装
## 环境
>  "vue": "^3.2.6"   "vite": "^2.5.4" 
## 在线地址
[https://blueskyliu.gitee.io/llt-admin-preview/#/dashboard/workbench](https://blueskyliu.gitee.io/llt-admin-preview/#/dashboard/workbench)
## 效果图
![在这里插入图片描述](https://img-blog.csdnimg.cn/8d676435651f4325947a0b969885ef86.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAQkxVRVNLWUhPU1Q=,size_20,color_FFFFFF,t_70,g_se,x_16)

![在这里插入图片描述](https://img-blog.csdnimg.cn/383b16d324eb40ad99bdc18ce020483c.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAQkxVRVNLWUhPU1Q=,size_20,color_FFFFFF,t_70,g_se,x_16)
## 引入

```js
import {calendarVue} from 'calendar-vue3'
import 'calendar-vue3/dist/style.css'
```
## 更新日志
2020-2-8
1.修复日期上个月样式bug
2.修复系统升级第三方UI产生的样式bug

next
1. 新增 气泡弹窗 徽章显示 日期里有多少事件
2. 处理"^1.3.0-beta.5", 版本太高 内部样式无法撑开

## 提示
如果无法安装请切换npm官方镜像
npm config set registry https://registry.npmjs.org/ 同步yarn 淘宝镜像需要时间 如果是历史版本就没问题

1. 提示esBuild.exe 建议重启项目就没问题了 或者删除原来的node_modules
## [文档地址](https://blueskyliu.gitee.io/vuepress-starter-blog/page/vue/vue3calendarVue.html#%E5%AE%89%E8%A3%85)

