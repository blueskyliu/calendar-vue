import { createApp } from 'vue'
import App from './App.vue'
import { setupRouter } from '/@ts/router'
import ElementPlus from 'element-plus'
import 'element-plus/theme-chalk/display.css';//1.1.0-beta.10 版本
import 'element-plus/dist/index.css'//1.1.0-beta.10 版本

import './init.scss';
// 国际化
import locale from 'element-plus/lib/locale/lang/zh-cn'
import 'dayjs/locale/zh-cn'
const app =createApp(App)
app.use(ElementPlus, { locale })//element-plus挂载
setupRouter(app)
app.mount('#app')
