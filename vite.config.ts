import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';//为打包后的文件提供传统浏览器兼容性支持
import legacy from '@vitejs/plugin-legacy';
import { resolve } from 'path';
import viteCompression from 'vite-plugin-compression';//压缩文件
// https://vitejs.dev/config/
function pathResolve(dir: string) {
  return resolve(process.cwd(), '.', dir);
}
export default defineConfig({
  base:"",
  build: {
    lib: {
      entry:resolve(__dirname, 'src/export.ts'),
      name: 'calendarVue3',
      fileName: (format) => `calendar-vue3.${format}.ts`
    },
    rollupOptions: {
      // 确保外部化处理那些你不想打包进库的依赖
      external: ['vue'],
      output: {
        // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
        globals: {
          vue: 'Vue'
        }
      }
    }
  },
  server:{
    host:"0.0.0.0",
    port:8080
  },
  resolve: {
    alias: [
      // /@/xxxx => src/xxxx
      {
        find: /\/@\//,
        replacement: pathResolve('src') + '/',
      },
      {
        find: /\/@ts\//,
        replacement: pathResolve('src') + '/',
      },
      // /#/xxxx => types/xxxx
      {
        find: /\/#\//,
        replacement: pathResolve('types') + '/',
      },
      // ['@vue/compiler-sfc', '@vue/compiler-sfc/dist/compiler-sfc.esm-browser.js'],
    ]
  },
  plugins: [
    vue(),
    viteCompression(),
    // legacy({
    //   targets: ['defaults', 'not IE 11']
    // }),
  //   visualizer({
  //     open: true,
  //     gzipSize: true,
  //     brotliSize: true,
  // })
  ]
})
